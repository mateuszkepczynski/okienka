

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class MovieTableModel extends AbstractTableModel {
	final String[] columnName = new String[]{"Tytu�","Rok","Ocena","Re�yser","Gatunek"};
	

	private List<Movie> listaFilmow;
	
	public MovieTableModel() {
		super();
		this.listaFilmow = new ArrayList<>();
	}
	public void addMovie(Movie m){
		listaFilmow.add(m);
		fireTableDataChanged();
	}
	public void removeMovie(int index){
		listaFilmow.remove(index);
		fireTableDataChanged();
	}

	public List<Movie> getListaFilmow() {
		return listaFilmow;
	}
	public void setListaFilmow(List<Movie> listaFilmow) {
		this.listaFilmow = listaFilmow;
	}
	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return columnName[column];
	}
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnName.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return listaFilmow.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Movie tmp = listaFilmow.get(rowIndex);
		// TODO Auto-generated method stub
		switch(columnIndex){
		case 0:
			return tmp.getTilte();
		case 1:
			return tmp.getYear();
		
		case 2:
			return tmp.getRate();
			
		case 3:
			return tmp.getDirector();
		
		case 4:
			return tmp.getGenre();
			
			
		}
		return null;
	}

}
