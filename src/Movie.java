
public class Movie {
private String tilte;
private String director;
private String year;
private String genre;
private float rate;

public Movie() {
	super();
}
//public Movie(String tilte, String director, String year, String genre, float rate) {
//	super();
//	this.tilte = tilte;
//	this.director = director;
//	this.year = year;
//	this.genre = genre;
//	this.rate = rate;
//}
public String getTilte() {
	return tilte;
}
public void setTilte(String tilte) {
	this.tilte = tilte;
}
public String getDirector() {
	return director;
}
public void setDirector(String director) {
	this.director = director;
}
public String getYear() {
	return year;
}
public void setYear(String year) {
	this.year = year;
}
public String getGenre() {
	return genre;
}
public void setGenre(String genre) {
	this.genre = genre;
}
public float getRate() {
	return rate;
}
public void setRate(float rate) {
	this.rate = rate;
}
	
}
