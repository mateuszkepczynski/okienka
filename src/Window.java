import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class Window {

	private JFrame frame;
	private JTextField fieldTilte;
	private JTextField fieldYear;
	private JTextField fieldDirector;
	private JTextField fieldRate;
	private JTextField fildGenre;
	private JTable table;
	private JButton del; 
	private MovieTableModel model;
	private int indexZaznaczony;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
		model = new MovieTableModel();
		table.setModel(model);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		ListSelectionModel modelZaznaczenia = table.getSelectionModel();
		modelZaznaczenia.addListSelectionListener(new ListSelectionListener(){

			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				indexZaznaczony = e.getFirstIndex();
				if(indexZaznaczony ==-1){
					del.setEnabled(false);
				}else{
					del.setEnabled(true);
				}
					
			}
			
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 505, 386);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frame.getContentPane().add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panelLeft = new JPanel();
		panelLeft.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(255, 0, 204)));
		panel.add(panelLeft);
		GridBagLayout gbl_panelLeft = new GridBagLayout();
		gbl_panelLeft.columnWidths = new int[]{0, 0};
		gbl_panelLeft.rowHeights = new int[]{245, 0, 0};
		gbl_panelLeft.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelLeft.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		panelLeft.setLayout(gbl_panelLeft);
		
		JPanel panelLeftTop = new JPanel();
		GridBagConstraints gbc_panelLeftTop = new GridBagConstraints();
		gbc_panelLeftTop.insets = new Insets(0, 0, 5, 0);
		gbc_panelLeftTop.fill = GridBagConstraints.BOTH;
		gbc_panelLeftTop.gridx = 0;
		gbc_panelLeftTop.gridy = 0;
		panelLeft.add(panelLeftTop, gbc_panelLeftTop);
		GridBagLayout gbl_panelLeftTop = new GridBagLayout();
		gbl_panelLeftTop.columnWidths = new int[] {0};
		gbl_panelLeftTop.rowHeights = new int[] {22, 24, 0, 0, 0, 36};
		gbl_panelLeftTop.columnWeights = new double[]{1.0};
		gbl_panelLeftTop.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		panelLeftTop.setLayout(gbl_panelLeftTop);
		
		JLabel lblNewLabel = new JLabel("Dane Filmu:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panelLeftTop.add(lblNewLabel, gbc_lblNewLabel);
		
		JPanel panelTytu≥ = new JPanel();
		GridBagConstraints gbc_panelTytu≥ = new GridBagConstraints();
		gbc_panelTytu≥.insets = new Insets(0, 0, 5, 0);
		gbc_panelTytu≥.fill = GridBagConstraints.BOTH;
		gbc_panelTytu≥.gridx = 0;
		gbc_panelTytu≥.gridy = 1;
		panelLeftTop.add(panelTytu≥, gbc_panelTytu≥);
		panelTytu≥.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_1 = new JLabel("Tytu\u0142:");
		lblNewLabel_1.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		panelTytu≥.add(lblNewLabel_1);
		
		fieldTilte = new JTextField();
		fieldTilte.setHorizontalAlignment(SwingConstants.CENTER);
		panelTytu≥.add(fieldTilte);
		fieldTilte.setColumns(10);
		
		JPanel panelRok = new JPanel();
		GridBagConstraints gbc_panelRok = new GridBagConstraints();
		gbc_panelRok.insets = new Insets(0, 0, 5, 0);
		gbc_panelRok.anchor = GridBagConstraints.NORTHWEST;
		gbc_panelRok.fill = GridBagConstraints.BOTH;
		gbc_panelRok.gridx = 0;
		gbc_panelRok.gridy = 2;
		panelLeftTop.add(panelRok, gbc_panelRok);
		panelRok.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_2 = new JLabel("Rok:");
		lblNewLabel_2.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.LEFT);
		panelRok.add(lblNewLabel_2);
		
		fieldYear = new JTextField();
		fieldYear.setHorizontalAlignment(SwingConstants.CENTER);
		panelRok.add(fieldYear);
		fieldYear.setColumns(10);
		
		JPanel panelRezyser = new JPanel();
		GridBagConstraints gbc_panelRezyser = new GridBagConstraints();
		gbc_panelRezyser.anchor = GridBagConstraints.NORTHWEST;
		gbc_panelRezyser.insets = new Insets(0, 0, 5, 0);
		gbc_panelRezyser.fill = GridBagConstraints.BOTH;
		gbc_panelRezyser.gridx = 0;
		gbc_panelRezyser.gridy = 3;
		panelLeftTop.add(panelRezyser, gbc_panelRezyser);
		panelRezyser.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_3 = new JLabel("Re\u017Cyser:");
		lblNewLabel_3.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		panelRezyser.add(lblNewLabel_3);
		
		fieldDirector = new JTextField();
		fieldDirector.setHorizontalAlignment(SwingConstants.CENTER);
		panelRezyser.add(fieldDirector);
		fieldDirector.setColumns(10);
		
		JPanel panelOcena = new JPanel();
		GridBagConstraints gbc_panelOcena = new GridBagConstraints();
		gbc_panelOcena.insets = new Insets(0, 0, 5, 0);
		gbc_panelOcena.anchor = GridBagConstraints.NORTHWEST;
		gbc_panelOcena.fill = GridBagConstraints.BOTH;
		gbc_panelOcena.gridx = 0;
		gbc_panelOcena.gridy = 4;
		panelLeftTop.add(panelOcena, gbc_panelOcena);
		panelOcena.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_4 = new JLabel("Ocena: ");
		lblNewLabel_4.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.LEFT);
		panelOcena.add(lblNewLabel_4);
		
		fieldRate = new JTextField();
		fieldRate.setHorizontalAlignment(SwingConstants.CENTER);
		panelOcena.add(fieldRate);
		fieldRate.setColumns(10);
		
		JPanel panelGatunek = new JPanel();
		GridBagConstraints gbc_panelGatunek = new GridBagConstraints();
		gbc_panelGatunek.fill = GridBagConstraints.BOTH;
		gbc_panelGatunek.gridx = 0;
		gbc_panelGatunek.gridy = 5;
		panelLeftTop.add(panelGatunek, gbc_panelGatunek);
		panelGatunek.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_5 = new JLabel("Gatunek:");
		panelGatunek.add(lblNewLabel_5);
		
		fildGenre = new JTextField();
		panelGatunek.add(fildGenre);
		fildGenre.setColumns(10);
		
		JPanel panelLeftBottom = new JPanel();
		GridBagConstraints gbc_panelLeftBottom = new GridBagConstraints();
		gbc_panelLeftBottom.fill = GridBagConstraints.BOTH;
		gbc_panelLeftBottom.gridx = 0;
		gbc_panelLeftBottom.gridy = 1;
		panelLeft.add(panelLeftBottom, gbc_panelLeftBottom);
		GridBagLayout gbl_panelLeftBottom = new GridBagLayout();
		gbl_panelLeftBottom.columnWeights = new double[]{0.0, 0.0};
		gbl_panelLeftBottom.rowWeights = new double[]{0.0, 0.0};
		panelLeftBottom.setLayout(gbl_panelLeftBottom);
		
		JButton add = new JButton("Dodaj");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Movie m = new Movie();
				
				m.setTilte(fieldTilte.getText());
				m.setYear(fieldYear.getText());
				m.setDirector(fieldDirector.getText());
				m.setGenre(fildGenre.getText());
				m.setRate(Float.parseFloat((fieldRate.getText())));
				model.addMovie(m);
				
			}
		});
		add.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		GridBagConstraints gbc_add = new GridBagConstraints();
		gbc_add.fill = GridBagConstraints.BOTH;
		gbc_add.insets = new Insets(0, 0, 5, 5);
		gbc_add.gridx = 0;
		gbc_add.gridy = 0;
		panelLeftBottom.add(add, gbc_add);
		
		del = new JButton("Usu\u0144");
		del.setEnabled(false);
		del.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			model.removeMovie(indexZaznaczony);
			}
		});
		del.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		GridBagConstraints gbc_del = new GridBagConstraints();
		gbc_del.fill = GridBagConstraints.BOTH;
		gbc_del.insets = new Insets(0, 0, 5, 5);
		gbc_del.gridx = 1;
		gbc_del.gridy = 0;
		panelLeftBottom.add(del, gbc_del);
		
		JButton save = new JButton("Save");
		save.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		GridBagConstraints gbc_save = new GridBagConstraints();
		gbc_save.fill = GridBagConstraints.BOTH;
		gbc_save.insets = new Insets(0, 0, 0, 5);
		gbc_save.gridx = 0;
		gbc_save.gridy = 1;
		panelLeftBottom.add(save, gbc_save);
		
		JButton load = new JButton("Load");
		load.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		GridBagConstraints gbc_load = new GridBagConstraints();
		gbc_load.fill = GridBagConstraints.BOTH;
		gbc_load.insets = new Insets(0, 0, 0, 5);
		gbc_load.gridx = 1;
		gbc_load.gridy = 1;
		panelLeftBottom.add(load, gbc_load);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(255, 0, 204)));
		panel.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_2.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}

}
